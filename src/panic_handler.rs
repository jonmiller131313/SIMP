use std::{backtrace::Backtrace, panic::PanicInfo, process::exit, sync::Arc};

use chrono::{Local, SecondsFormat};
use serenity::{
    all::{CreateMessage, Http},
    builder::CreateAttachment,
};
use tokio::{runtime::Handle, task};

use crate::config::PanicHookConfig;

pub fn setup_panic_hook(discord: Arc<Http>, panic_hook_info: PanicHookConfig) {
    log::info!("Setting panic hook");
    log::debug!("Panic hook data: {panic_hook_info:?}");

    // Leak the discord session info, never clean this up in case we double-panic during unwinding.
    // We exit() immediately after printing our backtrace anyway, so we don't care about cleaning up.
    // SAFETY: Converting from pointer to reference is safe given we just got the pointer.
    let discord = unsafe { Arc::into_raw(discord).as_ref().unwrap() };

    std::panic::set_hook(Box::new(move |info| panic_callback(discord, panic_hook_info, info)));

    log::trace!("Setting panic hook complete");
}

pub fn panic_callback(http: &Http, PanicHookConfig { channel_id, user_id }: PanicHookConfig, info: &PanicInfo) {
    let timestamp = Local::now();
    let backtrace = Backtrace::force_capture();
    log::error!("! ! ! PANIC ! ! !");

    let msg = format!(
        "{info}\nBacktrace @ {}:\n{backtrace}",
        timestamp.to_rfc3339_opts(SecondsFormat::Secs, false)
    );
    log::error!("{msg}");
    log::debug!("Sending discord message");

    let panic_message_sent = task::block_in_place(|| {
        Handle::current().block_on(async move {
            let attachment = CreateAttachment::bytes(msg.as_bytes(), "backtrace.txt");
            let message = CreateMessage::new().content(format!("I'm panicking <@{user_id}>!"));

            http.send_message(channel_id.into(), vec![attachment], &message).await
        })
    });
    if let Err(e) = panic_message_sent {
        log::error!("Unable to send discord message: {e:#?}");
        exit(-2);
    } else {
        log::trace!("Sending discord message complete");
        exit(-1);
    };
}
