use once_cell::sync::Lazy;
use regex::{Captures, Regex, Replacer};

use super::UrlReplacer;

pub struct Reddit;

impl UrlReplacer for Reddit {
    fn regex() -> &'static Regex {
        static RE: Lazy<Regex> = Lazy::new(|| {
            Regex::new(r"\bhttps?://(?:(?:www|old)\.)?reddit(?<after>\.com/[a-zA-Z0-9_/]+)(?:\?\S+)?").unwrap()
        });
        &RE
    }

    fn new() -> Self {
        Self
    }
}

impl Replacer for Reddit {
    fn replace_append(&mut self, caps: &Captures<'_>, dst: &mut String) {
        log::debug!("Reddit link fixed");
        caps.expand("https://old.rxddit${after}", dst);
    }
}
