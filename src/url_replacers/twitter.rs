use once_cell::sync::Lazy;
use regex::{Captures, Regex, Replacer};

use super::UrlReplacer;

pub struct Twitter;

impl UrlReplacer for Twitter {
    fn regex() -> &'static Regex {
        static RE: Lazy<Regex> = Lazy::new(|| {
            Regex::new(r"\bhttps?://(?:www\.)?(?:twitter|x)(?<after>\.com/[a-zA-Z0-9_/]+)(?:\?\S+)?").unwrap()
        });
        &RE
    }

    fn new() -> Self {
        Self
    }
}

impl Replacer for Twitter {
    fn replace_append(&mut self, caps: &Captures<'_>, dst: &mut String) {
        log::debug!("Twitter link fixed");
        caps.expand("https://vxtwitter${after}", dst);
    }
}
