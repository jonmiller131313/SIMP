use once_cell::sync::Lazy;
use regex::{Captures, Regex, Replacer};

use super::UrlReplacer;

pub struct Instagram;

impl UrlReplacer for Instagram {
    fn regex() -> &'static Regex {
        static RE: Lazy<Regex> = Lazy::new(|| {
            Regex::new(r"\bhttps?://(?:www\.)?instagram(?<after>\.com/[a-zA-Z0-9_/]+)(?:\?\S+)?").unwrap()
        });
        &RE
    }

    fn new() -> Self {
        Self
    }
}

impl Replacer for Instagram {
    fn replace_append(&mut self, caps: &Captures<'_>, dst: &mut String) {
        log::debug!("Instagram link fixed");
        caps.expand("https://ddinstagram${after}", dst);
    }
}
