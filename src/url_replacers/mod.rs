use std::borrow::Cow;

use regex::{Regex, Replacer};

pub mod instagram;
pub mod reddit;
pub mod tiktok;
pub mod twitter;

pub use self::{instagram::*, reddit::*, tiktok::*, twitter::*};

pub trait UrlReplacer: Replacer + Sized {
    fn regex() -> &'static Regex;

    fn new() -> Self;

    fn replace_url(msg: Cow<str>) -> Cow<str> {
        match Self::regex().replace_all(&msg, Self::new()) {
            Cow::Borrowed(_) => msg,
            Cow::Owned(new_msg) => {
                log::debug!("Fixed url: {msg:?} => {new_msg:?}");
                new_msg.into()
            }
        }
    }
}
