use once_cell::sync::Lazy;
use regex::{Captures, Regex, Replacer};

use super::UrlReplacer;

pub struct Tiktok;

impl UrlReplacer for Tiktok {
    fn regex() -> &'static Regex {
        static RE: Lazy<Regex> =
            Lazy::new(|| Regex::new(r"\bhttps?://(?:www\.)?tiktok(?<after>\.com/[a-zA-Z0-9_/@]+)(?:\?\S+)?").unwrap());
        &RE
    }

    fn new() -> Self {
        Self
    }
}

impl Replacer for Tiktok {
    fn replace_append(&mut self, caps: &Captures<'_>, dst: &mut String) {
        log::debug!("TikTok link fixed");
        caps.expand("https://vxtiktok${after}", dst);
    }
}
