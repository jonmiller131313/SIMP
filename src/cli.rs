use clap::Parser;

#[derive(Parser)]
#[command(version, about)]
pub struct CLIArgs {
    /// Sets the log level.
    #[arg(short, long, default_value = "info", value_parser = ["off", "error", "warn", "info", "debug", "trace"])]
    pub logging: String,

    /// Enable logs from dependencies.
    #[arg(long)]
    pub dep_logging: bool,
}
