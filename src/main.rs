use std::{borrow::Cow, str::FromStr};

use anyhow::{Context, Result};
use chrono::{Local, SecondsFormat};
use clap::Parser;
use log::LevelFilter;
use once_cell::sync::Lazy;
use serenity::{
    async_trait,
    builder::{CreateAllowedMentions, CreateMessage},
    client::Context as DiscordContext,
    model::prelude::*,
    prelude::*,
};

use crate::{
    cli::CLIArgs,
    config::{PanicHookConfig, SimpConfig},
    panic_handler::*,
    url_replacers::*,
};

mod cli;
mod config;
mod panic_handler;
mod url_replacers;

const HELP_MSG: &str = r"
I am a bot that helps inline media from social media posts.

My currently supported links:
- Instagram: https://github.com/Wikidepia/InstaFix
- Reddit: https://github.com/MinnDevelopment/fxreddit
- TikTok: https://github.com/dylanpdx/vxtiktok
- Twitter/X: https://github.com/dylanpdx/BetterTwitFix
";
const QUESTION_EMOJI: char = '❔';

struct Handler;
impl Handler {
    fn fix_msg(msg: &str) -> Option<String> {
        use Cow::Owned;

        let mut replaced_url = false;
        let mut msg = Cow::Borrowed(msg);

        // msg = Instagram::replace_url(msg);
        // if let Owned(_) = msg {
        //     replaced_url = true;
        // }

        msg = Reddit::replace_url(msg);
        if let Owned(_) = msg {
            replaced_url = true;
        }

        msg = Tiktok::replace_url(msg);
        if let Owned(_) = msg {
            replaced_url = true;
        }

        msg = Twitter::replace_url(msg);
        if let Owned(_) = msg {
            replaced_url = true;
        }

        if replaced_url {
            Some(msg.into())
        } else {
            None
        }
    }
}

// #[cfg(test)]
// mod tests {
//     use super::*;
//
//     #[test]
//     fn test() {
//         let msg = r"
//         ";
//
//         let fixed_msg = Handler::fix_msg(msg);
//         dbg!(&fixed_msg);
//         assert!(fixed_msg.is_some());
//     }
// }

#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, ctx: DiscordContext, orig_msg: Message) {
        static MESSAGE_BUILDER: Lazy<CreateMessage> =
            Lazy::new(|| CreateMessage::new().allowed_mentions(CreateAllowedMentions::new()));

        log::trace!("Handling message: {orig_msg:#?}");

        let Some(fixed_msg) = Self::fix_msg(&orig_msg.content) else {
            log::trace!("No social media links found, ignoring");
            return;
        };

        let message = MESSAGE_BUILDER
            .clone()
            .content(format!("<@{}>:\n{fixed_msg}", orig_msg.author.id));

        let msg = match orig_msg.channel_id.send_message(&ctx.http, message).await {
            Err(why) => {
                log::error!("Unable to send message: {why:#?}");
                return;
            }
            Ok(msg) => msg,
        };

        if let Err(why) = msg.react(&ctx.http, QUESTION_EMOJI).await {
            log::error!("Unable to add reaction: {why:#?}");
            return;
        } else if let Err(why) = orig_msg.delete(&ctx.http).await {
            log::error!("Unable to delete original message: {why:#?}");
            return;
        }

        log::trace!("Handling message complete");
    }

    async fn reaction_add(&self, ctx: DiscordContext, reaction: Reaction) {
        log::trace!("Handling reaction: {reaction:#?}");

        if reaction.emoji != QUESTION_EMOJI.into() {
            log::trace!("Not question mark, ignoring");
            return;
        }

        let current_user = ctx.cache.current_user().id;
        let Some(reaction_user) = reaction.user_id else {
            log::error!("No reaction user id?");
            return;
        };

        if reaction_user == current_user {
            log::trace!("Question mark emoji reacted by me, ignoring");
            return;
        }

        let msg = match reaction.message(&ctx.http).await {
            Err(why) => {
                log::error!("Unable to get reaction message: {why:#?}");
                return;
            }
            Ok(msg) => msg,
        };
        if msg.author.id != current_user {
            log::trace!("Message reacted to not mine, ignoring");
            return;
        }

        if let Err(why) = reaction
            .channel_id
            .say(&ctx.http, format!("Hi <@{reaction_user}>! {HELP_MSG}"))
            .await
        {
            log::error!("Unable to send help msg: {why:#?}");
            return;
        }

        log::trace!("Handling reaction complete");
    }

    async fn ready(&self, ctx: DiscordContext, _: Ready) {
        log::debug!("Connected to Discord");

        if let Some(panic_hook_info) = ctx.data.write().await.remove::<PanicHookConfig>() {
            setup_panic_hook(ctx.http, panic_hook_info);
        }
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let cli_args = CLIArgs::parse();

    init_logging(&cli_args);

    let SimpConfig { discord, panic_hook } = SimpConfig::try_load().context("Unable to load configuration")?;
    log::debug!("Discord token: {discord:?}");

    let intents =
        GatewayIntents::GUILD_MESSAGES | GatewayIntents::MESSAGE_CONTENT | GatewayIntents::GUILD_MESSAGE_REACTIONS;

    let mut c = Client::builder(discord.token, intents).event_handler(Handler);
    if let Some(panic_hook_info) = panic_hook {
        c = c.type_map_insert::<PanicHookConfig>(panic_hook_info);
    }

    c.await
        .context("Unable to create Discord client?")?
        .start()
        .await
        .context("Unable to start Discord client?")?;

    Ok(())
}

fn init_logging(args: &CLIArgs) {
    let level = LevelFilter::from_str(&args.logging).expect("Clap invalid parse log level?");

    let mut log_builder = env_logger::builder();
    log_builder.format_timestamp_secs().format(|f, record| {
        use std::io::Write;
        writeln!(
            f,
            "[{}] [{:<20}] {:>5}: {}",
            Local::now().to_rfc3339_opts(SecondsFormat::Secs, false),
            record.target(),
            record.level(),
            record.args()
        )
    });
    if !args.dep_logging {
        log_builder
            .filter_level(LevelFilter::Off)
            .filter_module(env!("CARGO_PKG_NAME"), level);
    } else {
        log_builder.filter_level(level);
    }
    log_builder.init();
}
