use std::{env, fmt};

use anyhow::{Context, Result};
use config::{Config, File};
use serde::Deserialize;
use serenity::prelude::TypeMapKey;
use xdg::BaseDirectories;

#[derive(Debug, Deserialize)]
pub struct SimpConfig {
    pub discord: DiscordConfig,
    pub panic_hook: Option<PanicHookConfig>,
}

#[derive(Deserialize)]
pub struct DiscordConfig {
    pub token: String,
}

#[derive(Copy, Clone, Debug, Deserialize)]
pub struct PanicHookConfig {
    pub channel_id: u64,
    pub user_id: u64,
}
impl TypeMapKey for PanicHookConfig {
    type Value = Self;
}

impl SimpConfig {
    pub fn try_load() -> Result<Self> {
        log::debug!("Loading configuration");

        match env::var("SIMP_DISCORD_TOKEN") {
            Ok(token) => {
                log::info!("Read Discord token from environment variable");
                let discord = DiscordConfig { token };
                log::debug!("Token: {discord:?}");

                return Ok(SimpConfig {
                    discord,
                    panic_hook: None,
                });
            }
            Err(env::VarError::NotPresent) => log::trace!("No Discord token environment variable found"),
            Err(env::VarError::NotUnicode(_)) => {
                log::warn!("Discord token from environment not unicode, ignoring")
            }
        };

        let cwd = env::current_dir().context("Unable to get current working directory")?;
        let xdg = BaseDirectories::new().context("Unable to get XDG directories")?;

        let mut config = Config::builder()
            .add_source(File::from(cwd.join("simp.toml")).required(false))
            .add_source(File::from(cwd.join("config.toml")).required(false));

        if let Some(file) = xdg.find_config_file("simp.toml") {
            log::trace!("Found config file in XDG config path");
            config = config.add_source(File::from(file).required(false));
        }

        let config = config.build()?.try_deserialize().context("Invalid toml");
        log::trace!("Loading configuration complete");
        config
    }
}

impl fmt::Debug for DiscordConfig {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.token.len() <= 5 {
            write!(f, "*****")
        } else {
            let width = self.token.len() - 4;
            write!(
                f,
                "{}{:*^width$}{}",
                &self.token[..2],
                '*',
                &self.token[self.token.len() - 2..]
            )
        }
    }
}
